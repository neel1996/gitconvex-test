#!/bin/bash

for((i=0;i<100;i++)); 
do 
	d=`date +%Y%m%d%H%M%S`
	echo $d > README.md
	git add --all
	git commit -m "Test Commit $d"
	git push -u origin master
	sleep 30
done
